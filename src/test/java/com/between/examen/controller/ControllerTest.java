package com.between.examen.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ControllerTest {

    @Autowired
    private MockMvc mvc;

    private final Long mockBrandId = 1L;

    private final Long mockProductId = 35455L;

    private final String requestUri = "/v1/brands/"+mockBrandId+"/products/"+mockProductId+"/prices";

    @Test
    @DisplayName(value = "Test 1: petición a las 10:00 del día 14 Jun del producto 35455 para la brand 1 (ZARA)")
    public void test1() throws Exception {
        String mockDateStr = "2020-06-14T10:00:00";
        BigDecimal expectedValue = BigDecimal.valueOf(35.50);

        mvc.perform(
                MockMvcRequestBuilders.get(requestUri)
                        .queryParam("applicationDate", mockDateStr)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(expectedValue));
    }

    @Test
    @DisplayName(value = "Test 2: petición a las 16:00 del día 14 Jun del producto 35455 para la brand 1 (ZARA)")
    public void test2() throws Exception {
        String mockDateStr = "2020-06-14T16:00:00";
        BigDecimal expectedValue = BigDecimal.valueOf(25.45);

        mvc.perform(
                MockMvcRequestBuilders.get(requestUri)
                        .queryParam("applicationDate", mockDateStr)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(expectedValue));
    }

    @Test
    @DisplayName(value = "Test 3: petición a las 21:00 del día 14 Jun del producto 35455 para la brand 1 (ZARA)")
    public void test3() throws Exception {
        String mockDateStr = "2020-06-14T21:00:00";
        BigDecimal expectedValue = BigDecimal.valueOf(35.50);

        mvc.perform(
                MockMvcRequestBuilders.get(requestUri)
                        .queryParam("applicationDate", mockDateStr)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(expectedValue));
    }

    @Test
    @DisplayName(value = "Test 4: petición a las 10:00 del día 15 Jun del producto 35455 para la brand 1 (ZARA)")
    public void test4() throws Exception {
        String mockDateStr = "2020-06-15T10:00:00";
        BigDecimal expectedValue = BigDecimal.valueOf(30.50);

        mvc.perform(
                MockMvcRequestBuilders.get(requestUri)
                        .queryParam("applicationDate", mockDateStr)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(expectedValue));
    }

    @Test
    @DisplayName(value = "Test 5: petición a las 21:00 del día 16 Jun del producto 35455 para la brand 1 (ZARA)")
    public void test5() throws Exception {
        String mockDateStr = "2020-06-16T21:00:00";
        BigDecimal expectedValue = BigDecimal.valueOf(38.95);

        mvc.perform(
                MockMvcRequestBuilders.get(requestUri)
                        .queryParam("applicationDate", mockDateStr)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(expectedValue));
    }

}