/**********************************/
CREATE TABLE IF NOT EXISTS brands (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    UNIQUE INDEX UNIQ_1C52F9585E237E06 (name),
    PRIMARY KEY(id)
);

/**********************************/
CREATE TABLE IF NOT EXISTS products (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    UNIQUE INDEX UNIQ_D34A04AD5E237E06 (name),
    PRIMARY KEY(id)
);

/**********************************/
CREATE TABLE IF NOT EXISTS prices_list (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    UNIQUE INDEX UNIQ_C34A04A87E237F09 (name),
    PRIMARY KEY(id)
);

/**********************************/
CREATE TABLE IF NOT EXISTS prices (
    id INT AUTO_INCREMENT NOT NULL,
    brand_id INT DEFAULT NULL,
    product_id INT DEFAULT NULL,
    price_list INT DEFAULT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    price DOUBLE PRECISION NOT NULL,
    currency VARCHAR(3) NOT NULL,
    priority INT NOT NULL,
    PRIMARY KEY(id)
);

/**********************************/

ALTER TABLE prices ADD CONSTRAINT FK_E5F1AC3344F5D008 FOREIGN KEY (brand_id) REFERENCES brands (id);
ALTER TABLE prices ADD CONSTRAINT FK_E5F1AC334584665A FOREIGN KEY (product_id) REFERENCES products (id);
ALTER TABLE prices ADD CONSTRAINT FK_E5F1AC3346968A9F FOREIGN KEY (price_list) REFERENCES prices_list (id);