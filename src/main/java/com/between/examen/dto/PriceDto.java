package com.between.examen.dto;

import com.between.examen.entity.Price;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class PriceDto implements Serializable {
    @JsonProperty(value = "brand_id")
    Long brandId;

    @JsonProperty(value = "product_id")
    Long productId;

    @JsonProperty(value = "tax_type")
    String taxType;

    @JsonProperty(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    Date startDate;

    @JsonProperty(value = "end_date")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    Date endDate;

    @JsonProperty(value = "price")
    BigDecimal price;

    @JsonProperty(value = "currency")
    String currency;


    public static PriceDto from(Price price) {
        ModelMapper modelMapper = new ModelMapper();
        TypeMap<Price, PriceDto> propertyMapper = modelMapper.createTypeMap(Price.class, PriceDto.class);
        propertyMapper.addMappings(
                mapper -> mapper.map(src -> src.getPriceList().getTaxType(), PriceDto::setTaxType)
        );
        return modelMapper.map(price, PriceDto.class);
    }
}
