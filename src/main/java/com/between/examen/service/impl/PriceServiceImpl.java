package com.between.examen.service.impl;

import com.between.examen.entity.Price;
import com.between.examen.repository.PriceRepository;
import com.between.examen.service.PriceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class PriceServiceImpl implements PriceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PriceServiceImpl.class);

    @Autowired
    private PriceRepository repository;

    @Override
    public Optional<Price> get(Long brandId, Long productId, Date applicationDate) {
        List<Price> prices = repository.findByBrandProductAndApplicationDate(
                brandId,
                productId,
                applicationDate
        );

        LOGGER.debug(prices.toString());
        return prices.stream().findFirst();
    }
}
