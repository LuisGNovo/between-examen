package com.between.examen.service;

import com.between.examen.entity.Price;

import java.util.Date;
import java.util.Optional;

public interface PriceService {
    Optional<Price> get(Long brandId, Long productId, Date applicationDate);
}
