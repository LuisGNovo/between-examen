package com.between.examen.entity;

import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "prices_list")
@Data
public class PriceList {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "tax_type")
    private String taxType;
}
