package com.between.examen.repository;

import com.between.examen.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("FROM Price p " +
            "JOIN Brand b ON b.id = :brandId " +
            "JOIN Product pr ON pr.id = :productId " +
            "WHERE :applicationDate BETWEEN p.startDate AND p.endDate " +
            "ORDER BY p.priority DESC")
    List<Price> findByBrandProductAndApplicationDate(
            @Param("brandId") Long brandId,
            @Param("productId") Long productId,
            @Param("applicationDate") Date applicationDate
    );
}
