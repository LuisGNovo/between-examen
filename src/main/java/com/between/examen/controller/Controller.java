package com.between.examen.controller;


import com.between.examen.dto.PriceDto;
import com.between.examen.entity.Price;
import com.between.examen.service.PriceService;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@Validated
@RequestMapping("/v1")
public class Controller {

    @Autowired
    private PriceService priceService;

    @GetMapping("/brands/{brandId}/products/{productId}/prices")
    @ResponseBody
    public ResponseEntity<PriceDto> getPricing(
            @PathVariable("brandId") Long brandId,
            @PathVariable("productId") Long productId,
            @Parameter(description = "Date format pattern is yyyy-MM-dd'T'HH:mm:ss")
            @RequestParam(value = "applicationDate")
            @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date applicationDate
            ) {

        Optional<Price> price = priceService.get(brandId, productId, applicationDate);
        if (price.isPresent()) {
            return ResponseEntity.ok(PriceDto.from(price.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
